//
//  RealmToDoTableViewCell.swift
//  Skillbox-1.14
//
//  Created by Rodion on 29.10.2021.
//

import UIKit

protocol CellDelegate: AnyObject {
    func deleteCell(cellID: Int)
    func updateCell(at index: Int)
}

class RealmToDoTableViewCell: UITableViewCell {
    private var cellID = Int()
    weak var delegate: CellDelegate?

    @IBOutlet weak var taskTextfield: UITextField!
 
    @IBAction func TextfieldEditingChanged(_ sender: Any) {
        RealmToDoPersistence.shared.updateTask(taskID: cellID, text: taskTextfield.text ?? "Not saved")
    }
    
    @IBAction func deleteTaskButton(_ sender: Any) {
        delegate?.deleteCell(cellID: cellID)
    }
    
    func getCellID(id: Int, text: String) {
        self.cellID = id
        self.taskTextfield.text = text
    }
}
