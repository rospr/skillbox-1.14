//
//  UserDefaultsPersistance.swift
//  Skillbox-1.14
//
//  Created by Rodion on 29.10.2021.
//

import Foundation

class UserDefaultsPersistance {
    static let shared = UserDefaultsPersistance()
    
    private let _nameKey = "UserDefaultsPersistance.nameKey"
    private let _surnameKey = "UserDefaultsPersistance.surnameKey"
    
    private let _weatherArrayKey = "UserDefaultsPersistance.weatherArrayKey"
    
    
    var name: String {
        set {
            UserDefaults.standard.set(newValue, forKey: _nameKey)
        }
        get {
            return UserDefaults.standard.string(forKey: _nameKey) ?? ""
        }
    }
    
    var surname: String {
        set {
            UserDefaults.standard.set(newValue, forKey: _surnameKey)
        }
        get {
            return UserDefaults.standard.string(forKey: _surnameKey) ?? ""
        }
    }
    
    
    var weatherArray: [[String: String]] {
        set {
//            UserDefaults.standard.removeObject(forKey: _weatherArrayKey)
            UserDefaults.standard.set(newValue, forKey: _weatherArrayKey)
        }
        get {
            return UserDefaults.standard.object(forKey: _weatherArrayKey) as? [[String : String]] ?? []
        }
    }
    
    func removeWeatherArray() {
        UserDefaults.standard.removeObject(forKey: _weatherArrayKey)
    }
    
}
