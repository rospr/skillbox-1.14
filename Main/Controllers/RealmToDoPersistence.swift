//
//  RealmToDoPersistence.swift
//  Skillbox-1.14
//
//  Created by Rodion on 29.10.2021.
//

import Foundation
import RealmSwift

class RealmToDoPersistence {
    static let shared = RealmToDoPersistence()
    private let realm = try! Realm()
    
    //MARK: - Функция добавления новой задачи
    func updateTasks(task_text: String) {
        let task_example = TasksRealmModel()
        task_example.task = task_text
        
        try! realm.write {
            realm.add(task_example)
        }
    }
    
    //MARK: - Функция получения всех сохранённых задач
    func getTasks() -> [String] {
        var tasks: [String] = []
        let savedTasks = realm.objects(TasksRealmModel.self)
        
        for task_example in savedTasks {
            tasks.append(task_example.task)
        }
        
        return tasks
    }
    
    //MARK: - Функция удаления задачи по её порядковому номеру
    func deleteTask(task_id: Int) {
        let taskToDelete = realm.objects(TasksRealmModel.self)[task_id]
        
        try! realm.write {
            realm.delete(taskToDelete)
        }
    }
    
    //MARK: - Функция добавления текста задачи
    func updateTask(taskID: Int, text: String) {
        let taskToUpdate = realm.objects(TasksRealmModel.self)[taskID]
        try! realm.write {
            taskToUpdate.task = text
        }
    }
}
