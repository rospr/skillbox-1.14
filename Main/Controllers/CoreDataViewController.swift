//
//  CoreDataViewController.swift
//  Skillbox-1.14
//
//  Created by Rodion on 10.11.2021.
//

import UIKit

class CoreDataViewController: UIViewController {
    @IBOutlet weak var tasksTableView: UITableView!
    
    var tasksArray: [Notes] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tasksTableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tasksArray = CoreDataToDoPersistence.shared.getTasks()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tasksTableView.dataSource = self
        tasksTableView.delegate = self
    }
    
    @IBAction func addNoteButton(_ sender: Any) {
        let alertController = UIAlertController(title: "Новая задача", message: "Введите задачу", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Отмена", style: .default)
        let save = UIAlertAction(title: "Сохранить", style: .default) { _ in
            let textfield = alertController.textFields?.first
            if let newTask = textfield?.text {
                if let task = CoreDataToDoPersistence.shared.saveTask(task: newTask) {
                    self.tasksArray.insert(task, at: 0)
                }
            }
        }
        
        alertController.addTextField { _ in }
        alertController.addAction(cancel)
        alertController.addAction(save)
        
        present(alertController, animated: true)
    }
}

//MARK: - Расширения для TableView
extension CoreDataViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tasksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CoreDataTableViewCell", for: indexPath) as! CoreDataTableViewCell
        cell.delegate = self
        
        let text = tasksArray[indexPath.row].note ?? ""
        cell.getCellID(id: indexPath.row, text: text)
        
        return cell
    }
    
    
}

extension CoreDataViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

//MARK: - Реализация протокола делегата от ячейки таблицы
extension CoreDataViewController: CoreDataTableViewCellDelegate {
    func deleteCell(cellID: Int) {
        CoreDataToDoPersistence.shared.deleteTask(task: self.tasksArray[cellID])
        self.tasksArray.remove(at: cellID)
    }
    
    func updateCell(at index: Int) {
        DispatchQueue.main.async {
            self.tasksTableView.reloadData()
        }
    }
}
