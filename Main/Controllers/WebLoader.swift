//
//  WebLoader.swift
//  Skillbox-1.14
//
//  Created by Rodion on 29.10.2021.
//

import Foundation
import Alamofire

class WebLoader {
    var currentWeatherDict: [String: String] = ["day_temp": "", "min_temp": "", "max_temp": "", "day_feels": "", "pressure": "", "humidity": "", "wind_speed": "", "description": ""]
    
//MARK: - Загрузка текущей погоды
    func loadCurrentWeather(completion: @escaping ([String: String]) -> Void) {
        let urlString = "https://api.openweathermap.org/data/2.5/weather?id=1508291&appid=d224a3500180779b62a9e9e32786639e&units=metric&lang=ru"
        AF.request(urlString).responseJSON { response in
            switch response.result {
            case .success(let value):
                DispatchQueue.global().async {
                    if let parsedWeather = CurrentWeatherModel(data: value as! NSDictionary) {
                        //MARK: - Описание погоды
                        if let weather = WeatherModel(parsedWeather: parsedWeather.weather.first!) {
                            self.currentWeatherDict["description"] = weather.description
                        }
                        //MARK: - Основные характеристики (температура, влажность, давление)
                        if let mainCharacteristics = MainWeatherCharacteristicsModel(parsedWeather: parsedWeather.main) {
                            self.currentWeatherDict["day_temp"] = String(mainCharacteristics.temp)
                            self.currentWeatherDict["day_feels"] = String(mainCharacteristics.feels_like)
                            self.currentWeatherDict["min_temp"] = String(mainCharacteristics.temp_min)
                            self.currentWeatherDict["max_temp"] = String(mainCharacteristics.temp_max)
                            self.currentWeatherDict["pressure"] = String(mainCharacteristics.pressure)
                            self.currentWeatherDict["humidity"] = String(mainCharacteristics.humidity)
                        }
                        //MARK: - Скорость ветра
                        if let wind = WindModel(parsedWeather: parsedWeather.wind) {
                            self.currentWeatherDict["wind_speed"] = String(wind.speed)
                        }
                        completion(self.currentWeatherDict)
                    }
                }
            default: return
            }
        }
    }
    
//MARK: -  Загрузка прогноза погоды на 5 дней вперёд
    func loadPredictedWeather(completion: @escaping ([[String: String]]) -> Void) {
        let urlString = "https://api.openweathermap.org/data/2.5/onecall?lat=60.5&lon=54.0&exclude=current,minutely,hourly,alerts&units=metric&lang=ru&appid=d224a3500180779b62a9e9e32786639e"
        AF.request(urlString).responseJSON { response in
            switch response.result {
            case .success(let value):
                DispatchQueue.global().async {
                    if let parsedWeather = PredictedWeatherModel(data: value as! NSDictionary) {
                        var predictedWeatherArray: [[String: String]] = []
                        for (index, daily) in parsedWeather.daily.enumerated() {
                            if index < 5 {
                                var predictedWeatherDict: [String: String] = ["day_temp": "", "min_temp": "", "max_temp": "", "day_feels": "", "pressure": "", "humidity": "", "wind_speed": "", "description": ""]
                                if let dailyWeather = DailyWeatherModel(daily: daily) {
                                    //MARK: - Температура
                                    if let temp = TempModel(dailyWeather: dailyWeather.temp) {
                                        predictedWeatherDict["day_temp"] = String(temp.day)
                                        predictedWeatherDict["min_temp"] = String(temp.min)
                                        predictedWeatherDict["max_temp"] = String(temp.max)
                                    }
                                    //MARK: - Ощущается как
                                    if let feels_like = FeelsLikeModel(dailyWeather: dailyWeather.feels_like) {
                                        predictedWeatherDict["day_feels"] = String(feels_like.day)
                                    }
                                    //MARK: - Давление, влажность, скорость ветра
                                    predictedWeatherDict["pressure"] = String(dailyWeather.pressure)
                                    predictedWeatherDict["humidity"] = String(dailyWeather.humidity)
                                    predictedWeatherDict["wind_speed"] = String(dailyWeather.wind_speed)
                                    //MARK: - Описание
                                    if let description = WeatherModel(parsedWeather: dailyWeather.weather.first!) {
                                        predictedWeatherDict["description"] = description.description
                                    }
                                    //MARK: - Добавление в массив
                                    predictedWeatherArray.append(predictedWeatherDict)
                                }
                            }
                        }
                        completion(predictedWeatherArray)
                    }
                }
            default: return
            }
        }
    }
}
