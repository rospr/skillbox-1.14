//
//  WeatherCollectionViewCell.swift
//  Skillbox-1.14
//
//  Created by Rodion on 01.11.2021.
//

import UIKit

class WeatherCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dayTempLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    
    func updateLabels(date: String, description: String, dayTemp: String, feelsLike: String, minTemp: String, maxTemp: String, pressure: String, humidity: String, windSpeed: String) {
        dateLabel.text = "\(date)"
        descriptionLabel.text = description
        dayTempLabel.text = "\(dayTemp)°С"
        feelsLikeLabel.text = "Ощущается как \(feelsLike)°С"
        minTempLabel.text = "Минимум: \(minTemp)°С"
        maxTempLabel.text = "Максимум: \(maxTemp)°С"
        pressureLabel.text = "Давление: \(pressure) мм.рт.ст."
        humidityLabel.text = "Влажность: \(humidity)%"
        windSpeedLabel.text = "Скорость ветра: \(windSpeed) м/с"
    }
    
    func updateView() {
        self.layer.cornerRadius = 12
        self.layer.borderWidth = 3
        self.layer.borderColor = UIColor.gray.cgColor
    }
}
