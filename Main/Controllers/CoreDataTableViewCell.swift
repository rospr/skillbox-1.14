//
//  CoreDataTableViewCell.swift
//  Skillbox-1.14
//
//  Created by Rodion on 11.11.2021.
//

import UIKit

protocol CoreDataTableViewCellDelegate: AnyObject {
    func deleteCell(cellID: Int)
}

class CoreDataTableViewCell: UITableViewCell {
    @IBOutlet weak var taskLabel: UILabel!
    
    private var cellID = Int()
    weak var delegate: CoreDataTableViewCellDelegate?
    
    @IBAction func deleteTaskButton(_ sender: Any) {
        delegate?.deleteCell(cellID: cellID)
    }
    
    func getCellID(id: Int, text: String) {
        self.cellID = id
        self.taskLabel.text = text
    }
}
