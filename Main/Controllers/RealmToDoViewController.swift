//
//  RealmToDoViewController.swift
//  Skillbox-1.14
//
//  Created by Rodion on 29.10.2021.
//

import UIKit

class RealmToDoViewController: UIViewController {

    @IBOutlet weak var realmToDoTableView: UITableView!
    
    var tasksArray: [String] = [] {
        didSet {
            DispatchQueue.main.async {
                self.realmToDoTableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        realmToDoTableView.dataSource = self
        realmToDoTableView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        insertSavedTasksInArray()
    }
    
    //MARK: - Кнопка добавления новой задачи: добавляется в верх таблицы, записывается в Realm
    @IBAction func addTaskButton(_ sender: Any) {
        tasksArray.insert("", at: 0)
        
        RealmToDoPersistence.shared.updateTasks(task_text: "Задача \(tasksArray.count)")
    }
    
    func insertSavedTasksInArray() {
        tasksArray = RealmToDoPersistence.shared.getTasks()
    }
}

//MARK: - Расширения для TableView
extension RealmToDoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoRealmCell", for: indexPath) as! RealmToDoTableViewCell
        cell.delegate = self

        let task_index = tasksArray.count - 1 - indexPath.row
        let text = RealmToDoPersistence.shared.getTasks()[task_index]
        cell.getCellID(id: indexPath.row, text: text)
        
        return cell
    }
}

extension RealmToDoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK: - Реализация протокола делегата от ячейки таблицы
extension RealmToDoViewController: CellDelegate {
    func deleteCell(cellID: Int) {
        let task_index = tasksArray.count - 1 - cellID

        RealmToDoPersistence.shared.deleteTask(task_id: task_index)
        
        insertSavedTasksInArray()
    }
    
    func updateCell(at index: Int) {
        self.realmToDoTableView.reloadData()
    }
}
