//
//  CoreDataToDoPersistence.swift
//  Skillbox-1.14
//
//  Created by Rodion on 21.11.2021.
//

import Foundation
import CoreData
import UIKit

class CoreDataToDoPersistence {
    static let shared = CoreDataToDoPersistence()
    
    //MARK: - Получение задач из CoreData
    func getTasks() -> [Notes] {
        var tasksArray: [Notes] = []
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest = Notes.fetchRequest()
        
        do {
            tasksArray = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return tasksArray
    }
    
    //MARK: - Сохранение задачи
    func saveTask(task: String) -> Notes? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Notes", in: context) else { return nil }
        
        let notesObject = Notes(entity: entity, insertInto: context)
        notesObject.note = task
        
        do {
            try context.save()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return notesObject
    }
 
    //MARK: - Удаление задачи
    func deleteTask(task: Notes) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext

        context.delete(task)
        
        do {
            try context.save()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
