//
//  WeatherViewController.swift
//  Skillbox-1.14
//
//  Created by Rodion on 01.11.2021.
//

import UIKit

class WeatherViewController: UIViewController {
    
    @IBOutlet weak var weatherCollectionView: UICollectionView!
    
    var parsedWeatherArray: [[String: String]] = []
    var weatherArray: [[String: String]] = [] {
        didSet {
            DispatchQueue.main.async {
                self.weatherCollectionView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        weatherCollectionView.dataSource = self
        weatherCollectionView.delegate = self
        
        self.weatherArray = UserDefaultsPersistance.shared.weatherArray
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fetchPredictedWeather()
    }
    
    //MARK: - Парсинг данных погоды
    func fetchPredictedWeather() {
        self.parsedWeatherArray = []
        
        
        WebLoader().loadCurrentWeather { currentWeather in
            self.parsedWeatherArray.append(currentWeather)
        }
        
        WebLoader().loadPredictedWeather { predictedWeather in
            for weather in predictedWeather {
                self.parsedWeatherArray.append(weather)
            }
        
            self.weatherArray = []
            UserDefaultsPersistance.shared.removeWeatherArray()
            for weather in self.parsedWeatherArray {
                self.weatherArray.append(weather)
                UserDefaultsPersistance.shared.weatherArray.append(weather)
            }
        }
    }
}


//MARK: - Расширения для TableView
extension WeatherViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weatherArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = weatherCollectionView.dequeueReusableCell(withReuseIdentifier: "WeatherCollectionViewCell", for: indexPath) as! WeatherCollectionViewCell
        
        let calendar = Calendar.current
        let today = Date()
        let tomorrow = calendar.date(byAdding: .day, value: indexPath.row, to: today)!
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.YYYY"
        let date = formatter.string(from: tomorrow as Date)
        
        let weather_info = weatherArray[indexPath.row]
        
        cell.updateView()
        cell.updateLabels(date: date, description: weather_info["description"] ?? "", dayTemp: weather_info["day_temp"] ?? "", feelsLike: weather_info["day_feels"] ?? "", minTemp: weather_info["min_temp"] ?? "", maxTemp: weather_info["max_temp"] ?? "", pressure: weather_info["pressure"] ?? "", humidity: weather_info["humidity"] ?? "", windSpeed: weather_info["wind_speed"] ?? "")
        
        return cell
    }
}


extension WeatherViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width
        return CGSize(width: width - 30, height: width * 0.7)
    }
}
