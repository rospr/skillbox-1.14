//
//  UserDefaultsViewController.swift
//  Skillbox-1.14
//
//  Created by Rodion on 29.10.2021.
//

import UIKit

class UserDefaultsViewController: UIViewController {
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var surnameTextfield: UITextField!
    
    @IBAction func saveButton(_ sender: Any) {
        UserDefaultsPersistance.shared.name = nameTextfield.text ?? ""
        UserDefaultsPersistance.shared.surname = surnameTextfield.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        nameTextfield.text = UserDefaultsPersistance.shared.name
        surnameTextfield.text = UserDefaultsPersistance.shared.surname
    }
}
