//
//  WeatherModel.swift
//  Skillbox-1.14
//
//  Created by Rodion on 28.10.2021.
//

import Foundation

class WeatherModel {
    let description: String
    
    init?(parsedWeather: NSDictionary) {
        guard let description = parsedWeather["description"] as? String else { return nil }
        
        self.description = description
    }
}
