//
//  PredictedWeatherModel.swift
//  Skillbox-1.14
//
//  Created by Rodion on 28.10.2021.
//

import Foundation


class PredictedWeatherModel {
    let lat: Double
    let lon: Double
    let timezone: String
    let timezone_offset: Int
    let daily: [NSDictionary]
    
    init?(data: NSDictionary) {
        guard let lat = data["lat"] as? Double,
              let lon = data["lon"] as? Double,
              let timezone = data["timezone"] as? String,
              let timezone_offset = data["timezone_offset"] as? Int,
              let daily = data["daily"] as? [NSDictionary] else { return nil }
        self.lat = lat
        self.lon = lon
        self.timezone = timezone
        self.timezone_offset = timezone_offset
        self.daily = daily
    }
}
