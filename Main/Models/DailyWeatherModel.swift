//
//  DailyWeatherModel.swift
//  Skillbox-1.14
//
//  Created by Rodion on 28.10.2021.
//

import Foundation

class DailyWeatherModel {
    let temp: NSDictionary
    let feels_like: NSDictionary
    let pressure: Int
    let humidity: Int
    let wind_speed: Double
    let weather: [NSDictionary]
    
    init?(daily: NSDictionary) {
        guard let temp = daily["temp"] as? NSDictionary,
              let feels_like = daily["feels_like"] as? NSDictionary,
              let pressure = daily["pressure"] as? Int,
              let humidity = daily["humidity"] as? Int,
              let wind_speed = daily["wind_speed"] as? Double,
              let weather = daily["weather"] as? [NSDictionary] else { return nil }
        self.temp = temp
        self.feels_like = feels_like
        self.pressure = pressure
        self.humidity = humidity
        self.wind_speed = wind_speed
        self.weather = weather
    }
}
