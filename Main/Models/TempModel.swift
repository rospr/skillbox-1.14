//
//  TempModel.swift
//  Skillbox-1.14
//
//  Created by Rodion on 28.10.2021.
//

import Foundation

class TempModel {
    let day: Double
    let min: Double
    let max: Double
    let night: Double
    let eve: Double
    let morn: Double
    
    init?(dailyWeather: NSDictionary) {
        guard let day = dailyWeather["day"] as? Double,
              let min = dailyWeather["min"] as? Double,
              let max = dailyWeather["max"] as? Double,
              let night = dailyWeather["night"] as? Double,
              let eve = dailyWeather["eve"] as? Double,
              let morn = dailyWeather["morn"] as? Double else { return nil }
        self.day = day
        self.min = min
        self.max = max
        self.night = night
        self.eve = eve
        self.morn = morn
    }
}
