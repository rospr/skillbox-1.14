//
//  WindModel.swift
//  Skillbox-1.14
//
//  Created by Rodion on 28.10.2021.
//

import Foundation

class WindModel {
    let speed: Int
    
    init?(parsedWeather: NSDictionary) {
        guard let speed = parsedWeather["speed"] as? Int else { return nil }
        
        self.speed = speed
    }
}
