//
//  FeelsLikeModel.swift
//  Skillbox-1.14
//
//  Created by Rodion on 28.10.2021.
//

import Foundation

class FeelsLikeModel {
    let day: Double
    let night: Double
    let eve: Double
    let morn: Double
    
    init?(dailyWeather: NSDictionary) {
        guard let day = dailyWeather["day"] as? Double,
              let night = dailyWeather["night"] as? Double,
              let eve = dailyWeather["eve"] as? Double,
              let morn = dailyWeather["morn"] as? Double else { return nil }
        self.day = day
        self.night = night
        self.eve = eve
        self.morn = morn
    }
}
