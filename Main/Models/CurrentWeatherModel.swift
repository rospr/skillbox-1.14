//
//  CurrentWeatherModel.swift
//  Skillbox-1.14
//
//  Created by Rodion on 28.10.2021.
//

import Foundation

class CurrentWeatherModel {
    let coord: NSDictionary
    let weather: [NSDictionary]
    let base: String
    let main: NSDictionary
    let visibility: Int
    let wind: NSDictionary
    let clouds: NSDictionary
    let dt: Int
    let sys: NSDictionary
    let timezone: Int
    let id: Int
    let name: String
    let cod: Int
    
    init?(data: NSDictionary) {
        guard let coord = data["coord"] as? NSDictionary,
              let weather = data["weather"] as? [NSDictionary],
              let base = data["base"] as? String,
              let main = data["main"] as? NSDictionary,
              let visibility = data["visibility"] as? Int,
              let wind = data["wind"] as? NSDictionary,
              let clouds = data["clouds"] as? NSDictionary,
              let dt = data["dt"] as? Int,
              let sys = data["sys"] as? NSDictionary,
              let timezone = data["timezone"] as? Int,
              let id = data["id"] as? Int,
              let name = data["name"] as? String,
              let cod = data["cod"] as? Int else { return nil }
        self.coord = coord
        self.weather = weather
        self.base = base
        self.main = main
        self.visibility = visibility
        self.wind = wind
        self.clouds = clouds
        self.dt = dt
        self.sys = sys
        self.timezone = timezone
        self.id = id
        self.name = name
        self.cod = cod
    }
}
