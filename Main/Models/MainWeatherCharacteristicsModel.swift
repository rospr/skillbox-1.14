//
//  MainWeatherCharacteristicsModel.swift
//  Skillbox-1.14
//
//  Created by Rodion on 28.10.2021.
//

import Foundation

class MainWeatherCharacteristicsModel {
    let temp: Double
    let feels_like: Double
    let temp_min: Double
    let temp_max: Double
    let pressure: Int
    let humidity: Int
    
    init?(parsedWeather: NSDictionary) {
        guard let temp = parsedWeather["temp"] as? Double,
              let feels_like = parsedWeather["feels_like"] as? Double,
              let temp_min = parsedWeather["temp_min"] as? Double,
              let temp_max = parsedWeather["temp_max"] as? Double,
              let pressure = parsedWeather["pressure"] as? Int,
              let humidity = parsedWeather["humidity"] as? Int else { return nil }
        self.temp = temp
        self.feels_like = feels_like
        self.temp_min = temp_min
        self.temp_max = temp_max
        self.pressure = pressure
        self.humidity = humidity
    }
}
